# bioluminescence spectra #
For an up-to-date list of luminous species, see the [website of Professor Yuichi Oba](https://www3.chubu.ac.jp/faculty/oba_yuichi/living_light_list/)

# Frequency of colors across luminous animals #
![hastings_1996_color_summary_recolored_v2.png](https://bitbucket.org/wrf/biolum-spectra/raw/d5ed5026c5afe233dea8cfb79c2c31c8174eb2d9/hastings_1996_color_summary_recolored_v2.png)

recolored from Hastings (1996) [Chemistries and colors of bioluminescent reactions: a review](https://doi.org/10.1016/0378-1119(95)00676-1) Gene 173:1 5-11.

# Bioluminescence spectra of cnidarians #
![cnidarian_spectra_v2.png](https://bitbucket.org/wrf/biolum-spectra/raw/8b4b853164e542daef32b538cdb7254436123984/cnidarian_spectra_v2.png)

* The sea pen *Pennatula phosphorea*, by [Nicol 1958](https://doi.org/10.1017/S0025315400005610), and the same species again, by [Widder 1983](https://doi.org/10.2307/1541479)
* The sea pansy *Renilla reniformis*, by [Cormier 1962](https://www.sciencedirect.com/science/article/pii/0006300262907424)
* Some pennatulacea, *Acanthoptilum gracile*, *Ptilosarcus gurneyi*, *Stylatula elongata*, digitized from [Wampler 1973](https://doi.org/10.1016/0005-2728(73)90068-6)
* A related sea pansy *Renilla kollikeri*, by [Widder 1983](https://doi.org/10.2307/1541479)
* Sea pen *Stachyptilum superbum*, by [Widder 1983](https://doi.org/10.2307/1541479)
* The zoantharian *Parazoanthus lucificum* with two different colony colors, by [Widder 1983](https://doi.org/10.2307/1541479)
* bioluminescence spectra from the deep-sea medusa species *Aegina citrea*, *Aeginura grimaldii*, *Atolla parva*, *Atolla vanhoeffeni*, *Atolla wyvillei*, *Halicreas minimum*, *Halitrephes valdiviae*, *Periphylla periphylla*, *Periphyllopsis braueri*, *Poralia sp*, *Solmissus incisa*, *Solmundella bitentaculata*, by [Haddock 1999](http://dx.doi.org/10.1007/s002270050497)
* siphonophores *Craseoa lathetica*, *Nectadamas diomedeae*, *Rosacea plicata*, *Vogtia glabra*, by [Haddock 1999](http://dx.doi.org/10.1007/s002270050497)
* Seven octocorals: *Lepidisis sp*, *Isidella tentaculum*, *Heteropolypus ritteri*, *Umbellula sp*, *Distichoptilum gracile*, *Funiculina sp*, and a hormathiid anemone, from [Bessho-Uehara 2020](https://doi.org/10.1007/s00227-020-03706-ws)
* Spectra (GFP-type fluorescence only) of 3 sea pens, *Pennatula rubra*, *Pteroeides griseum*, and *Veretillum cynomorium*, my preprint is up here [Francis 2020](https://doi.org/10.1101/2020.12.08.416396), and see the [accompanying video](https://bitbucket.org/wrf/biolum-spectra/downloads/francis2020_pennatulacea_biolum_compilation_v1.mp4) of the bioluminescence in real time, following stimulation with KCl

# Bioluminescence spectra of ctenophores #
* bioluminescence spectra from *Bathocyroe fosteri*, *Bathyctena stellata*, *Beroe ovata*, *Bolinopsis vitrea*, *Cestum veneris*, *Euplokamis stationis*, *Eurhamphaea vexilligera*, *Lampea lactea*, *Thalassocalyce inconstans*, by [Haddock 1999](http://dx.doi.org/10.1007/s002270050497)
* recombinant photoprotein from the deep-sea ctenophore *Bathocyroe fosteri*, by [Powers 2013](http://dx.doi.org/10.1016/j.bbrc.2012.12.026), lmax at 486nm

# Bioluminescence spectra of molluscs #
* Bioluminescence spectra of *Pholas dactylus*, digitized from [Nicol 1958](http://dx.doi.org/10.1017/S0025315400014806), lmax at 484nm
* Bioluminescence spectra of *Watasenia scintillans*, digitized from [Teranishi 2008](http://dx.doi.org/10.1016/j.bbagen.2008.01.016), lmax at 470nm
* Bioluminescence spectra of *Abraliopsis sp*, lmax at 476nm

# Bioluminescence spectra of polychaetes #
![figure-3-v2_color.png](https://bitbucket.org/repo/aj849a/images/919806017-figure-3-v2_color.png)

* In vivo spectra from *Tomopteris sp*, *Poeobius meseres*, and *Flota flabelligera* from Francis, Powers, and Haddock (2016) [Bioluminescence spectra from three deep-sea polychaete worms.](https://link.springer.com/article/10.1007%2Fs00227-016-3028-2) Marine Biology 163: 255, [free preprint here](https://www.researchgate.net/publication/309557299_Bioluminescence_spectra_from_three_deep-sea_polychaete_worms)
* Bioluminescence and fluorescence spectra from *Tomopteris sp*, by [Francis 2014](https://doi.org/10.1002/bio.2671)
* Pelagic polychaete *Tomopteris nisseni* from [Latz 1988](http://dx.doi.org/10.1007/bf00391120)
* Tube worm *Chaetopterus variopedatus* from [Nicol 1957a](https://doi.org/10.1017/S0025315400025893)
* Polynoid worms *Gattyana cirrosa*, *Harmothoe longisetis*, *Lagisca extenuata*, and *Polynoe scolopendrina* from [Nicol 1957b](http://dx.doi.org/10.1017/S0025315400025820)
* Fireworm *Odontosyllis enopla* from [Shimomura 1963](http://doi.wiley.com/10.1002/jcp.1030610309)
* *Polycirrus perplexus* from [Huber 1989](http://www.ingentaconnect.com/content/umrsmas/bullmar/1989/00000044/00000003/art00013)

# Bioluminescence spectra of fish #
![combined_fish_spectra_v1.png](https://bitbucket.org/wrf/biolum-spectra/raw/c561c3eb120ffe85b347677524b61a02797f2e37/combined_fish_spectra_v1.png)

* the lantern fish *Myctophum punctatum*, by [Nicol 1960](https://doi.org/10.1017/S0025315400013072)
* Posterior photophores of the hatchetfish *Argyropelecus affinis*, by [Widder 1983](https://doi.org/10.2307/1541479)
* the pineapple fish *Cleidopus gloria-marus*, by [Widder 1983](https://doi.org/10.2307/1541479)
* the suborbital (red) and postorbital (green) photophores of *Aristostomias scintillans*, by [Widder 1984](https://doi.org/10.1126/science.225.4661.512)
* suborbital (red) and postorbital (green) photophores of *Malacosteus niger*, as well as suborbital with covering tissue removed, also by [Widder 1984](https://doi.org/10.1126/science.225.4661.512)

# Visual sensitivity of crustaceans #
* the lobster *Homarus vulgaris*, by [Kampa 1963](https://doi.org/10.1017/S0025315400025613) 
* the crab *Pleuroncodes planipes*, by [Fernandez 1973](https://doi.org/10.1007/BF00351453)

# Visual sensitivity of fish #
* collection of visual sensitivity peaks from 213 ray-finned fish, by [Schweikert 2018](https://doi.org/10.1242/jeb.189761)

# Notes #
Raw spectra can be found in the [source](https://bitbucket.org/wrf/biolum-spectra/src) section.

Many spectra were digitized from the original papers using [DataThief](http://www.datathief.org/)


